package parsers_sms;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

import entities.NormalizedSetInMessage;
import entities.Parameter_Parser;
import entities.Parser;
import entities.SetInMessage;
import lombok.Data;
import lombok.extern.java.Log;

@Data
@Log
public class ParserAlgo {

    /**
     * 1) get the good config parser from setinmessage
     * 
     * 2) work with the configs founded
     * 
     * 
     */

    private NormalizedSetInMessage normalizedSetInMessage;

    private Parser parser;

    private List<String> message_tokens;

    private String first_word;

    private Pattern pattern;

    private Matcher matcher;

    private Map<String, String> props_founded;

    public NormalizedSetInMessage parse_sms() {
        props_founded = new HashMap<>();
        String before_founded_in_text = null;
        String after_founded_in_text = null;
        String value_founded = null;
        boolean stop_search_before = false;
        boolean stop_search_after = false;
        message_tokens = new ArrayList<>();
        String[] msg_tokens_temp = getNormalizedSetInMessage().getSetIn_message().get().getSetIn_msg_body().split(" ");

        for (String token : msg_tokens_temp) {
            message_tokens.add(token);
        }
        getNormalizedSetInMessage().getSetIn_message().get().setSetIn_msg_body(
                removeAccents(getNormalizedSetInMessage().getSetIn_message().get().getSetIn_msg_body()));
        first_word = (((Pattern.compile("\\d+[,]*").matcher(message_tokens.get(0)).find())) ? "FLOAT"
                : message_tokens.get(0)) + normalizedSetInMessage.getSetIn_message().get().getSetIn_msg_from()
                + normalizedSetInMessage.getSetIn_message().get().getCountry_code_iso2();
        parser = ofy().load().type(Parser.class).filter("firsts_words", first_word).first().now();
        if (parser != null) {
            for (Parameter_Parser param_parser : parser.getParameters()) {
                stop_search_before = false;
                stop_search_after = false;
                for (String before : param_parser.getBefore()) {
                    if (stop_search_before)
                        break;
                    if (!before.equals("")) {
                        // log.info("before : " + before);
                        pattern = Pattern.compile(before);
                        matcher = pattern
                                .matcher(getNormalizedSetInMessage().getSetIn_message().get().getSetIn_msg_body());
                        if (matcher.find()) {
                            before_founded_in_text = getNormalizedSetInMessage().getSetIn_message().get()
                                    .getSetIn_msg_body().substring(matcher.start(), matcher.end());
                            // log.info("before_founded : "+(before == before_founded));
                            for (String after : param_parser.getAfter()) {
                                if (stop_search_after)
                                    break;
                                if (!after.equals("")) {
                                    pattern = Pattern.compile(after);
                                    matcher = pattern.matcher(
                                            getNormalizedSetInMessage().getSetIn_message().get().getSetIn_msg_body());
                                    if (matcher.find()) {
                                        after_founded_in_text = getNormalizedSetInMessage().getSetIn_message().get()
                                                .getSetIn_msg_body().substring(matcher.start(), matcher.end());
                                        log.info("before founded " + before_founded_in_text);
                                        log.info("before founded " + parser.getId());
                                        String[] value = getNormalizedSetInMessage().getSetIn_message().get()
                                                .getSetIn_msg_body().split(before);
                                        log.info("property : " + param_parser.getName());
                                        if (value.length > 1 && !value[1].isEmpty()) {
                                            try {
                                                value_founded = value[1].split(after)[0].replace(" ", "");
                                                log.info("before founded : " + before_founded_in_text
                                                        + " | after founded " + after_founded_in_text
                                                        + " | value founded : " + value_founded);
                                                // log.info("prop : " + param_parser.getName());
                                                stop_search_after = true;
                                                stop_search_before = true;
                                                switch (param_parser.getName()) {
                                                case "amount":
                                                    getNormalizedSetInMessage().setAmount(moneyFormat(value_founded));
                                                    props_founded.put("amount",
                                                            getNormalizedSetInMessage().getAmount().toString());
                                                    for (String currency : parser.getCurrencies()) {
                                                        pattern = Pattern.compile(currency);
                                                        matcher = pattern.matcher(getNormalizedSetInMessage()
                                                                .getSetIn_message().get().getSetIn_msg_body());
                                                        if (matcher.find()) {
                                                                log.info("before : "+before);
                                                                log.info("after : "+after);
                                                                log.info("currency : "+currency);
                                                                log.info("j'yyyyyyyy ssuiiiiiiiiiiiss et c'eeeeesssstt : "+((before.split(currency).length > 0) || (after.split(currency).length > 0)));
                                                            if (((before.split(currency).length > 0) || (after.split(currency).length > 0))){
                                                                getNormalizedSetInMessage()
                                                                        .setCurrency(parser.getCurrencies().get(
                                                                                parser.getCurrencies().size() - 1));
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    break;
                                                case "commission":
                                                    getNormalizedSetInMessage()
                                                            .setCommission(moneyFormat(value_founded));
                                                    props_founded.put("commission",
                                                            getNormalizedSetInMessage().getCommission().toString());
                                                    break;
                                                case "fees":
                                                    getNormalizedSetInMessage().setFees(moneyFormat(value_founded));
                                                    props_founded.put("fees",
                                                            getNormalizedSetInMessage().getFees().toString());
                                                    break;
                                                case "receiverName":
                                                    getNormalizedSetInMessage()
                                                            .setMsg_receiver_name(takeOnlyName(value_founded));
                                                    props_founded.put("receiverName",
                                                            getNormalizedSetInMessage().getMsg_receiver_name());
                                                    break;
                                                case "receiverPhone":
                                                    getNormalizedSetInMessage().setReceiver_phone_number(
                                                            takeOnlyPhoneNumber(value_founded));
                                                    props_founded.put("receiverPhone",
                                                            getNormalizedSetInMessage().getReceiver_phone_number());
                                                    break;
                                                case "senderName":
                                                    getNormalizedSetInMessage()
                                                            .setMsg_sender_name(takeOnlyName(value_founded));
                                                    props_founded.put("senderName",
                                                            getNormalizedSetInMessage().getMsg_sender_name());
                                                    break;
                                                case "senderPhone":
                                                    getNormalizedSetInMessage()
                                                            .setSender_phone_number(takeOnlyPhoneNumber(value_founded));
                                                    props_founded.put("senderPhone",
                                                            getNormalizedSetInMessage().getSender_phone_number());
                                                    break;
                                                case "transactionId":
                                                    getNormalizedSetInMessage().setTransaction_id(value_founded);
                                                    props_founded.put("transactionId",
                                                            getNormalizedSetInMessage().getTransaction_id());
                                                    break;
                                                case "balance":
                                                    getNormalizedSetInMessage().setBalance(moneyFormat(value_founded));
                                                    props_founded.put("balance",
                                                            getNormalizedSetInMessage().getBalance().toString());
                                                    break;
                                                case "soldeRecharge":
                                                    getNormalizedSetInMessage()
                                                            .setSoldeRecharge(moneyFormat(value_founded));
                                                    props_founded.put("soldeRecharge",
                                                            getNormalizedSetInMessage().getSoldeRecharge().toString());
                                                    break;
                                                }
                                            } catch (ArrayIndexOutOfBoundsException exceptionArraySplit) {

                                                log.info("before parser config for " + param_parser.getName() + " : "
                                                        + before);
                                                log.info("before_founded for " + param_parser.getName() + " : "
                                                        + before_founded_in_text);
                                                log.info("after parser config for " + param_parser.getName() + " : "
                                                        + after);
                                                log.info("after_founded for " + param_parser.getName() + " : "
                                                        + after_founded_in_text);
                                                props_founded.clear();
                                                exceptionArraySplit.printStackTrace();
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (props_founded.isEmpty()) {
                log.info("normalize null");
                return null;
            } else {
                return getNormalizedSetInMessage();
            }
        } else {
            parser = new Parser();
            parser.setDate(new Date());
            parser.setFirsts_words(first_word);
            JsonParser j_parser = new JsonParser();
            Object all_currencies = null;
            try {
                all_currencies = j_parser.parse(new FileReader("countries_data/currencies_sms.json"));
            } catch (JsonIOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonSyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            JsonObject all_currencies_json = (JsonObject) all_currencies;

            String[] currencies_split = all_currencies_json.getAsJsonObject("countries_currencies")
                    .get(getNormalizedSetInMessage().getSetIn_message().get().getCountry_code_iso2()).getAsString().replaceAll(" ", "")
                    .split(",");
            for (String curr : currencies_split) {
                parser.getCurrencies().add(curr);
            }
            Key<Parser> id_parse = ofy().save().entity(parser).now();
            SetInMessage setin_msg_update = ofy().load().type(SetInMessage.class)
                    .id(getNormalizedSetInMessage().getSetIn_message().get().getId()).now();
            setin_msg_update.setId_parser(Ref.create(id_parse));
            ofy().save().entity(setin_msg_update).now();
            return null;
        }

    }

    // it's used to change format of amount
    private static double moneyFormat(String number) {
        double changeAmount = 0.0;
        if (number != null && number.contains(",")) {
            String sArray[] = number.split(",");
            String split = sArray[1];

            int count = 0;

            // Counts each character except space
            for (int i = 0; i < split.length(); i++) {
                if (split.charAt(i) != ' ')
                    count++;
            }
            if (count > 2) {
                log.info("TEST : " + count);
                changeAmount = Double.parseDouble(number.replace(",", ""));

                log.info("ChangeAmount : " + changeAmount);

            } else {
                changeAmount = Double.parseDouble(number.replace(",", "."));

                log.info("ChangeAmount : " + changeAmount);
            }
        }

        else if (number != null && number.contains(".")) {

            String sArray[] = number.split("\\.");
            String split = sArray[1];

            int count = 0;

            // Counts each character except space
            for (int i = 0; i < split.length(); i++) {
                if (split.charAt(i) != ' ')
                    count++;
            }
            if (count > 2) {
                log.info("Value of Count : " + count);
                changeAmount = Double.parseDouble(number.replace(".", ""));

                log.info("changeAmount : " + changeAmount);

            } else {
                changeAmount = Double.parseDouble(number);

                log.info("changeAmount : " + changeAmount);
            }
        } else if (number.contains("-")) {
            changeAmount = 00.00;
        } else {
            changeAmount = Double.parseDouble(number);
        }

        return changeAmount;
    }

    // it's used to take only phone in string like this : "695940770 VOUZAN"
    private static String takeOnlyPhoneNumber(String givenPhoneNumber) {
        String goodPhoneNumber = "";
        if (givenPhoneNumber != null) {
            if (givenPhoneNumber.matches("^[0-9]*$") && givenPhoneNumber.length() > 2) {
                log.info("PHONE NUMBER IN takeOnlyPhoneNumber METHOD: " + givenPhoneNumber);
                goodPhoneNumber = givenPhoneNumber;

            } else {
                log.info("GOOD PHONE NUMBER: " + givenPhoneNumber.replaceAll("\\D+", ""));
                goodPhoneNumber = givenPhoneNumber.replaceAll("\\D+", "");
            }
        }

        return goodPhoneNumber;
    }

    // it's used to take only name (sender, receiver etc..) in string like this :
    // "695940770 VOUZAN"
    private static String takeOnlyName(String givenName) {
        String goodName = "";
        if (givenName != null) {
            if (givenName.matches(".*\\d.*")) {
                log.info("GOOD NAME: " + givenName.replaceAll("[^a-zA-Z]", " "));

                goodName = givenName.replaceAll("[^a-zA-Z]", " ").trim();
            }

            else {
                log.info(" GIVEN NAME IN takeOnlyName METHOD :" + givenName);
                goodName = givenName;
            }
        }
        return goodName;
    }

    public static String removeAccents(String text) {
        return text == null ? null
                : Normalizer.normalize(text, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}