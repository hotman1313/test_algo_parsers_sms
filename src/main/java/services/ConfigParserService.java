package services;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.Key;

import org.json.JSONObject;

import entities.Parameter_Parser;
import entities.Parser;
import lombok.Data;
import lombok.extern.java.Log;

@Data
@Log
public class ConfigParserService {

    private Parser parser_final;
    private JSONObject properties; // the json string that contains befores and afters props

    public Long save(Parser parser) {
        Key<Parser> id_save = null;
        /**
         * 1. test if there is the same first_word in datastore if there is the same
         * first_word, add to it just the befores afters if there is not the same
         * first_word in datastore, create a new config parser 2. parse the properties
         * to a json object 3. serialize them to a parameters_parser objet 4. add to
         * datastore
         */

        // test if there exists a same first_word

        Parser parser_first_word_like = ofy().load().type(Parser.class).filter("firsts_words", parser.getFirsts_words())
                .first().now();

        if (parser_first_word_like != null) {
            parser_properties(parser_first_word_like, true);
            setParser_final(parser_first_word_like);
            id_save = ofy().save().entity(getParser_final()).now();
        } else {
            id_save = ofy().save().entity(parser).now();
            setParser_final(ofy().load().key(id_save).now());
            parser_properties(parser, false);
            setParser_final(parser);
            id_save = ofy().save().entity(getParser_final()).now();
        }
        return (id_save == null) ? null : new Long(id_save.getId());
        /*
         * for (String obj : properties.keySet()) {
         * log.info("prop name ("+obj+")"+" ["+properties.getJSONObject(obj).getString(
         * "before")+"]"+" | "+" ["+properties.getJSONObject(obj).getString("after")+"]"
         * ); } log.info("properties size : "+properties.keySet().size()); return null;
         */
    }

    private void parser_properties(Parser parser, boolean config_exists) {
        List<Parameter_Parser> new_params_for_exist = new ArrayList<>();
        List<String> props_name_in_exist = new ArrayList<>();
        if (config_exists) {
            for (Parameter_Parser parser_exist : parser.getParameters()) {
                props_name_in_exist.add(parser_exist.getName());
            }
            for (String prop_name : properties.keySet()) {
                if (props_name_in_exist.contains(prop_name)) {
                    for (Parameter_Parser param_parser : parser.getParameters()) {
                        if (param_parser.getName().equalsIgnoreCase(prop_name)) {
                            if (!param_parser.getBefore()
                                    .contains(properties.getJSONObject(prop_name).getString("before"))
                                    && !properties.getJSONObject(prop_name).getString("before").equals("")) {
                                param_parser.getBefore().add(properties.getJSONObject(prop_name).getString("before"));
                            }
                            if (!param_parser.getAfter()
                                    .contains(properties.getJSONObject(prop_name).getString("after"))
                                    && !properties.getJSONObject(prop_name).getString("after").equals("")) {
                                param_parser.getAfter().add(properties.getJSONObject(prop_name).getString("after"));
                            }
                        }
                    }
                } else {
                    Parameter_Parser parameter = new Parameter_Parser();
                    parameter.setName(prop_name);
                    if (!properties.getJSONObject(prop_name).getString("before").equals(""))
                        parameter.getBefore().add(properties.getJSONObject(prop_name).getString("before"));
                    if (!properties.getJSONObject(prop_name).getString("after").equals(""))
                        parameter.getAfter().add(properties.getJSONObject(prop_name).getString("after"));
                    new_params_for_exist.add(parameter);
                }
            }
            for (Parameter_Parser param_to_add : new_params_for_exist) {
                parser.getParameters().add(param_to_add);
            }
        } else {
            for (String prop_name : properties.keySet()) {
                Parameter_Parser parameter = new Parameter_Parser();
                parameter.setName(prop_name);
                if (!properties.getJSONObject(prop_name).getString("before").equals(""))
                    parameter.getBefore().add(properties.getJSONObject(prop_name).getString("before"));
                if (!properties.getJSONObject(prop_name).getString("after").equals(""))
                    parameter.getAfter().add(properties.getJSONObject(prop_name).getString("after"));
                parser.getParameters().add(parameter);
            }
        }
    }
    // for (Parameter_Parser param_parser : parser.getParameters()) {
    // if (properties.keySet().contains(param_parser.getName())) {
    // if
    // (!param_parser.getBefore().contains(properties.getJSONObject(param_parser.getName()).getString("before")))
    // {
    // param_parser.getBefore().add(properties.getJSONObject(param_parser.getName()).getString("before"));
    // }
    // if
    // (!param_parser.getAfter().contains(properties.getJSONObject(param_parser.getName()).getString("after")))
    // {
    // param_parser.getAfter().add(properties.getJSONObject(param_parser.getName()).getString("after"));
    // }
    // } else {
    // log.info("new prop in the exist one");
    // Parameter_Parser parameter = new Parameter_Parser();
    // parameter.setName(prop_name);
    // parameter.getBefore().add(properties.getJSONObject(prop_name).getString("before"));
    // parameter.getAfter().add(properties.getJSONObject(prop_name).getString("after"));
    // new_params_for_exist.add(parameter);
    // }
    // }
    // }
    // for (Parameter_Parser parameter_parser_to_add : new_params_for_exist) {
    // log.info("name :2 " + parameter_parser_to_add.getName());
    // parser.getParameters().add(parameter_parser_to_add);
    // }

    // } else {
    // for (String prop_name : properties.keySet()) {
    // Parameter_Parser parameter = new Parameter_Parser();
    // parameter.setName(prop_name);
    // parameter.getBefore().add(properties.getJSONObject(prop_name).getString("before"));
    // parameter.getAfter().add(properties.getJSONObject(prop_name).getString("after"));
    // parser.getParameters().add(parameter);
    // }
    // }

    public Parser load(Long uid) {
        return ofy().load().type(Parser.class).id(uid.toString()).now();
    }
}