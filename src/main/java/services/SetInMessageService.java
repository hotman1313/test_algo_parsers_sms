package services;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

import entities.NormalizedSetInMessage;
import entities.SetInMessage;
import lombok.Data;
import lombok.extern.java.Log;
import parsers_sms.ParserAlgo;
import tools.SUID;
import tools.Status;

@Data
@Log
public class SetInMessageService {

        private SetInMessage setInMessage;
        private NormalizedSetInMessage normalizedSetInMessage;

    /**
     * parse (normalize)
     * load
     * save entity
     */

    public Long save(SetInMessage msg) {
        /**
         * set date
         * set uid
         * set status
         */
        SUID uid_setin = new SUID();
        msg.setSetIn_msg_date(new Date());
        msg.setSetIn_msg_uid(uid_setin.get().toString());
        msg.setStatus(Status.SMS_NOT_PARSED.toString());

        //save in datastore
        Key<SetInMessage> id_save = ofy().save().entity(msg).now();
        SetInMessage setInMessage_to_load = load(id_save.getId());
        log.info("setinmessage to save is null ? : "+(setInMessage_to_load==null));
        setSetInMessage(setInMessage_to_load);

        //prepare the normalize
        normalizedSetInMessage = new NormalizedSetInMessage();
        setNormalizedSetInMessage(normalizedSetInMessage);
        getNormalizedSetInMessage().setSetIn_message(Ref.create(getSetInMessage()));

        return getSetInMessage().getId();
    }

    public SetInMessage load(long uid) {
        return ofy().load().type(SetInMessage.class).id(uid).now();
    }

    public NormalizedSetInMessage parse() {
        ParserAlgo parserAlgo = new ParserAlgo();
        getNormalizedSetInMessage().setDate(new Date());
        parserAlgo.setNormalizedSetInMessage(getNormalizedSetInMessage());

        setNormalizedSetInMessage(parserAlgo.parse_sms());

        if(getNormalizedSetInMessage() != null){
            setSetInMessage(getNormalizedSetInMessage().getSetIn_message().get());
            SetInMessage setin_msg_toUpdate_status = ofy().load().type(SetInMessage.class).id(getSetInMessage().getId()).now();
            setSetInMessage(setin_msg_toUpdate_status);
            getSetInMessage().setStatus(Status.SMS_PARSED.toString());
            ofy().delete().entity(getSetInMessage()).now();
            getSetInMessage().setId_parser(Ref.create(parserAlgo.getParser()));
            ofy().save().entity(getSetInMessage()).now();
            getNormalizedSetInMessage().setSetIn_message(Ref.create(getSetInMessage()));
            Key<NormalizedSetInMessage> id_save = ofy().save().entity(getNormalizedSetInMessage()).now();
            setNormalizedSetInMessage(ofy().load().key(id_save).now());
            log.info("normalize is null? :"+(getNormalizedSetInMessage()==null));
            getNormalizedSetInMessage().setSetIn_message(null);
            return getNormalizedSetInMessage();
        }else{
            return null;
        }

    }
}