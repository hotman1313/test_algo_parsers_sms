package servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.googlecode.objectify.ObjectifyService;

import entities.NormalizedSetInMessage;
import entities.Parser;
import entities.Personnage;
import entities.SetInMessage;
import lombok.extern.java.Log;

@SuppressWarnings("serial")
@Log
public class InitAppListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent ev) {
        // TODO Auto-generated method stub
        ObjectifyService.init();
        ObjectifyService.register(Personnage.class);
        ObjectifyService.register(SetInMessage.class);
        ObjectifyService.register(Parser.class);
        ObjectifyService.register(NormalizedSetInMessage.class);
        log.info("Application Start !");

    }

}