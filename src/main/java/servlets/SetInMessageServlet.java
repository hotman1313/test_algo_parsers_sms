package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.googlecode.objectify.Ref;

import org.json.JSONObject;

import entities.NormalizedSetInMessage;
import entities.SetInMessage;
import lombok.Data;
import lombok.extern.java.Log;
import services.SetInMessageService;

@Data
@Log
public class SetInMessageServlet extends HttpServlet {

    private SetInMessageService setInMessageService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // TODO Auto-generated method stub

        SetInMessage setInMessage = null;
        String userAgent = req.getHeader("User-Agent");
        log.warning("userAgent : " + userAgent);

        req.setCharacterEncoding("UTF-8"); // Encoding to UTF-8

        JSONObject jsonResponse = setBadResponse(req, resp,null);

        /**
         * 1. get params 2. constructs the setinmessage 3. save 4. normalize 5. send
         * response
         */

        String provider_name = null, merchant_agent_imei = null, setIn_msg_from = null, setIn_msg_message_id = null,
                setIn_msg_bridge_phone_number = null, setIn_msg_device_id = null, setIn_msg_sent_timestamp = null,
                setIn_msg_body = null, User_Agent = null, Agent_uid = null, setIn_origin_agent_phone_number = null;

        if (req.getParameter("provider_name") != null) {
            provider_name = req.getParameter("provider_name").toLowerCase();
        }
        if (req.getParameter("merchant_agent_imei") != null) {
            merchant_agent_imei = req.getParameter("merchant_agent_imei").toUpperCase();

        }
        if (req.getParameter("setIn_msg_from") != null) {
            setIn_msg_from = req.getParameter("setIn_msg_from").toUpperCase();
        }
        if (req.getParameter("setIn_msg_body") != null) {
            setIn_msg_body = req.getParameter("setIn_msg_body").toUpperCase();

        }
        if (req.getParameter("setIn_msg_device_id") != null) {
            setIn_msg_device_id = req.getParameter("setIn_msg_device_id").toUpperCase();

        }
        if (req.getParameter("User-Agent") != null) {
            User_Agent = req.getParameter("User-Agent").toUpperCase();
        }

        /**
         * construction of the setinmsg
         */
        setInMessage = new SetInMessage();
        setInMessage.setProvider_name(provider_name);
        setInMessage.setMerchant_agent_imei(merchant_agent_imei);
        setInMessage.setSetIn_msg_from(setIn_msg_from);
        setInMessage.setSetIn_msg_body(setIn_msg_body);
        setInMessage.setSetIn_msg_device_id(setIn_msg_device_id);
        setInMessage.setCountry_code_iso2(setInMessage.getSetIn_msg_device_id().substring(0, 2).toUpperCase());
        setInMessage.setUserAgent(userAgent);

        getSetInMessageService().save(setInMessage);
        NormalizedSetInMessage normalizedSetInMessage = getSetInMessageService().parse();
        log.info("normalize is null? :"+(normalizedSetInMessage==null));

        if (normalizedSetInMessage != null) {
            JSONObject normalize_to_joson = new JSONObject(normalizedSetInMessage);
            log.info("ooooooookkkkkkkkkkkkkkkkk");

            jsonResponse = setGoodResponse(req, resp, normalize_to_joson);

        }else{
            JSONObject json_msg_orror = new JSONObject();
            json_msg_orror.put("Message", "The message was not parsed");
            jsonResponse = setBadResponse(req, resp, json_msg_orror);
        }
            com.google.gson.JsonParser parser2 = new com.google.gson.JsonParser();
            com.google.gson.JsonObject jObject = parser2.parse(jsonResponse.toString()).getAsJsonObject();
            Gson prettyGson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
            String prettyJson = prettyGson.toJson(jObject);
            PrintWriter out = resp.getWriter();
            out.write(prettyJson);
    }

    private JSONObject setBadResponse(HttpServletRequest req, HttpServletResponse response,
            JSONObject responseContent) {
        int responseCode = 400;
        String responseStatus = "failed";
        String responseDetails = "Bad req";
        JSONObject jsonTestErrors = new JSONObject();

        return setResponse(req, response, responseCode, responseContent, responseStatus, responseDetails,
                jsonTestErrors);
    }

    private JSONObject setGoodResponse(HttpServletRequest req, HttpServletResponse response,
            JSONObject responseContent) {
        int responseCode = 201;
        String responseStatus = "success";
        String responseDetails = "Created";
        JSONObject jsonTestErrors = new JSONObject();

        return setResponse(req, response, responseCode, responseContent, responseStatus, responseDetails,
                jsonTestErrors);
    }

    private JSONObject setResponse(HttpServletRequest req, HttpServletResponse response, int responseCode,
            JSONObject responseContent, String responseStatus, String responseDetails, JSONObject jsonTestErrors) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(responseCode);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject jsonResponse = new JSONObject();

        jsonResponse.put("response_code", responseCode);
        jsonResponse.put("response_content", responseContent);
        jsonResponse.put("response_status", responseStatus);
        jsonResponse.put("response_errors", jsonTestErrors);
        jsonResponse.put("response_details", responseDetails);

        req.setAttribute("jsonResponse", jsonResponse);
        req.setAttribute("responseCode", responseCode);
        req.setAttribute("responseContent", responseContent);
        req.setAttribute("responseStatus", responseStatus);
        req.setAttribute("response-details", responseDetails);
        req.setAttribute("jsonTestErrors", jsonTestErrors);

        return jsonResponse;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.setInMessageService = new SetInMessageService();
    }
}