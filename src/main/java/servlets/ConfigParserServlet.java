package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import entities.Parser;
import lombok.Data;
import lombok.extern.java.Log;
import services.ConfigParserService;

@Data
@Log
public class ConfigParserServlet extends HttpServlet {

    private ConfigParserService configParserService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        JSONObject propertiesParser_json;
        Parser parser;
        Parser parser_config = null;
        String userAgent = req.getHeader("User-Agent");
        log.warning("userAgent : " + userAgent);

        req.setCharacterEncoding("UTF-8"); // Encoding to UTF-8
        resp.setContentType("application/json");

        JSONObject jsonResponse = setBadResponse(req, resp);

        /**
         * 1. get params 2. constructs the parser 3. the service add it to datastore 4.
         * send response
         */

        /**
         * 1. get params
         */
        String first_words = null, properties = null, currencies=null;

        if (req.getParameter("first_words") != null) {
            first_words = req.getParameter("first_words");
        }

        if (req.getParameter("properties") != null) {
            properties = req.getParameter("properties");
        }

        if (req.getParameter("currencies") != null) {
            currencies = req.getParameter("currencies");
        }
        /**
         * 2. constructs the parser
         */
        parser = new Parser();
        parser.setDate(new Date());
        parser.setFirsts_words(first_words);
        String[] currencies_split = currencies.split(",");
        for (String curr : currencies_split) {
            parser.getCurrencies().add(curr);
        }
        for (String string : parser.getCurrencies()) {
            log.info("currencies that i receive : "+string);
        }
        /**
         * convert properties parser parameter to jsonobject
         */

        propertiesParser_json = new JSONObject(properties);
        getConfigParserService().setProperties(propertiesParser_json);
        if(getConfigParserService().save(parser) != null){
            jsonResponse = setGoodResponse(req, resp);
            String parser_in_json = new Gson().toJson(getConfigParserService().getParser_final());
            JSONObject j = new JSONObject(parser_in_json);
            jsonResponse.put("parser", j);
        }else{
            jsonResponse = setBadResponse(req, resp);
            JSONObject j = new JSONObject();
            j.put("message", "there is not this first in datastore");
            jsonResponse.put("parser", j);

        }



        com.google.gson.JsonParser parser2 = new com.google.gson.JsonParser();
        com.google.gson.JsonObject jObject = parser2.parse(jsonResponse.toString()).getAsJsonObject();
        Gson prettyGson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        String prettyJson = prettyGson.toJson(jObject);
        PrintWriter out = resp.getWriter();
        out.write(prettyJson);
        

        /**
         * 3. the service add it to datastore
         */

        // getConfigParserService().save(parser);

    }

    private JSONObject setBadResponse(HttpServletRequest req, HttpServletResponse response) {
        int responseCode = 400;
        JSONObject responseContent = new JSONObject();
        String responseStatus = "failed";
        String responseDetails = "Bad req";
        JSONObject jsonTestErrors = new JSONObject();

        return setResponse(req, response, responseCode, responseContent, responseStatus, responseDetails,
                jsonTestErrors);
    }

    private JSONObject setGoodResponse(HttpServletRequest req, HttpServletResponse response) {
        int responseCode = 201;
        JSONObject responseContent = new JSONObject();
        String responseStatus = "success";
        String responseDetails = "Created";
        JSONObject jsonTestErrors = new JSONObject();

        return setResponse(req, response, responseCode, responseContent, responseStatus, responseDetails,
                jsonTestErrors);
    }

    private JSONObject setResponse(HttpServletRequest req, HttpServletResponse response, int responseCode,
            JSONObject responseContent, String responseStatus, String responseDetails, JSONObject jsonTestErrors) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(responseCode);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject jsonResponse = new JSONObject();

        jsonResponse.put("response_code", responseCode);
        jsonResponse.put("response_content", responseContent);
        jsonResponse.put("response_status", responseStatus);
        jsonResponse.put("response_errors", jsonTestErrors);
        jsonResponse.put("response_details", responseDetails);

        req.setAttribute("jsonResponse", jsonResponse);
        req.setAttribute("responseCode", responseCode);
        req.setAttribute("responseContent", responseContent);
        req.setAttribute("responseStatus", responseStatus);
        req.setAttribute("response-details", responseDetails);
        req.setAttribute("jsonTestErrors", jsonTestErrors);

        return jsonResponse;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.configParserService = new ConfigParserService();
    }

}