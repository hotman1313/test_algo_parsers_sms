package servlets;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import lombok.extern.java.Log;
import tools.WrappedReequest;

@Log
public class FillConfigParserServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("on y est");
        Map<String, String[]> params = new TreeMap<String, String[]>();
        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("wcu_parserConfig.json"));
        Object obj2 = parser.parse(new FileReader("countries_data/currencies_sms.json"));
        JsonObject oo = (JsonObject) obj2;

        JsonArray jsonObject = (JsonArray) obj;
        log.info("taaaaaaiilllllleeee "+oo.getAsJsonObject("countries_currencies").get("CM"));
        Iterator<JsonElement> it = jsonObject.iterator();
        while (it.hasNext()) {
            JsonElement el = it.next();
            /* log.info("first_words : "+el.getAsJsonObject().get("first_words").getAsString());
            log.info("parser_properties : "+el.getAsJsonObject().get("parser_properties"));*/
            String a = el.getAsJsonObject().get("first_words").getAsString().substring(0, (el.getAsJsonObject().get("first_words").getAsString().length()-2)); 
            
            JsonObject b = el.getAsJsonObject().get("parser_properties").getAsJsonObject();
            String[] ap = new String[1];
            String[] bp = new String[1];
            String[] cp = new String[1];
            ap[0] = a;
            bp[0] = b.toString();
            cp[0] = oo.getAsJsonObject("countries_currencies").get(ap[0].substring(ap[0].length()-2)).getAsString().replaceAll(" ", "");
            params.put("first_words", ap);
            params.put("properties", bp);
            params.put("currencies", cp);
            WrappedReequest wrappedReequest = new WrappedReequest(req, params);
            req.getRequestDispatcher("/configParser").forward(wrappedReequest, resp);
            
        }
    }
    
}