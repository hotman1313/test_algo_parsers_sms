/**
 * 
 */
package tools;

/**
 * @author Cedric ATANGANA
 *
 */
public enum Status {
	INACTIVE,
	VERIFIED,
	INCOMPLETE,
	PATCHED,
    ACTIVE,
    DESACTIVATED,
    BLACKLISTED,
    TOVALIDATE,
    PENDING,
    PAID,
    FAILED,
    CANCELED,
    TOREFUND,
    REFUNDED,
    REFUNDING,
    ADDED,
    FINISHED,
    EXPIRED,
    DELETED,
    SMS_PARSED,
    SMS_NOT_PARSED; //for sms not normalized
}
