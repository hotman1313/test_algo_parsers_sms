/**
 * 
 */
package tools;

/**
 * @author Cedric ATANGANA
 *
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class SUID {
	
	public SUID(){
		
	}
	private static final long xFFFFFF = 0xFFFFFF;
	private static final int xFF = 0xFF;

	private static final DateFormat SDF_MED = SimpleDateFormat.getDateTimeInstance( //
			SimpleDateFormat.MEDIUM, //
			SimpleDateFormat.MEDIUM);
	private static final SUID[] INSTANCES = new SUID[xFF + 1];

	private final AtomicLong INC = new AtomicLong();
	Random randomno = new Random();
	int temp =  (int) new Date().getTime() + 1;
	//private int instanceId = ThreadLocalRandom.current().nextInt(0, (int) new Date().getTime() + 1); // instanceId for different applications
	private int instanceId = randomno.nextInt(Math.abs(temp)); // instanceId for different applications

	static { // initiate 0-255 instances, to avoid duplication
		for (int i = 0; i <= xFF; i++) {
			SUID instance = new SUID();
			instance.instanceId = i;
			INSTANCES[i] = instance;
		}
	}

	public Long get() {
		return ((System.currentTimeMillis() >> 10) << 32) // timestamp
				+ ((INC.incrementAndGet() & xFFFFFF) << 8) // auto incremental
				+ instanceId // instance id
		;
	}

	
}