package entities;

import com.googlecode.objectify.annotation.*;

import lombok.Data;

@Data
@Entity
@Index
public class Personnage {
    @Id private Long id;
    private String nom;
    private int niveau;
    private int vie;

}