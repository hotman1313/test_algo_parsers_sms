package entities;

import java.util.Date;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.annotation.Parent;

import lombok.Data;

@Data
@Entity
@Index
@Cache
public class NormalizedSetInMessage {

    private @Id Long id;
	private Date date;
    private @Parent @Load Ref<SetInMessage> setIn_message;
	private String sender_currency;
	private String transaction_id;
	private String status;
	private Double amount;
	private Double fees;
	private Double balance;
	private Double soldeRecharge;
	private Double net_amount;
	private Double new_solde_amount;
	private String msg_sender_name;
	private String sender_phone_number;
	private String msg_receiver_name;
	public String receiver_phone_number;
	public String currency;
	private Double commission;
}