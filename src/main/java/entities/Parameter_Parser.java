package entities;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Parameter_Parser {

    private String name;
    private List<String> before;
    private List<String> after;

    public Parameter_Parser(){
        before = new ArrayList<>();
        after = new ArrayList<>();
    }

}