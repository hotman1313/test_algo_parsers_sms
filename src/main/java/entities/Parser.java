package entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import lombok.Data;

@Data
@Entity
@Index
@Cache
public class Parser {

    private @Id Long id;
    private String firsts_words;
    private List<Parameter_Parser> parameters;
    private List<String> currencies;
    private Date date;

    public Parser(){
        parameters = new ArrayList<>();
        currencies = new ArrayList<>();
    }
}