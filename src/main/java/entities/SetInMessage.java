package entities;

import java.util.Date;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import lombok.Data;

@Data
@Entity
@Index
@Cache
public class SetInMessage {

    private @Id Long id;

    private String setIn_msg_uid;
    private String userAgent; //from where the rquest arrives ok
    private String setIn_msg_from; // ok
    private String setIn_msg_device_id; // the country code ISO2 : CM-1 ok
    private String setIn_msg_body; // message from telcom ok
    private String merchant_agent_imei; // ok
    private String country_code_iso2; // ok
    private Date setIn_msg_date; // 01/11/1991 00:00:01
    private String status; // INACTIVE (by default) - ACTIVE - DESACTIVATED - DELETED - BLACKLISTED
    private String provider_name; // ok
    private String merchant_uid;
    private @Parent Ref<Parser> id_parser;
}